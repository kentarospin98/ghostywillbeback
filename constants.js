const DOWN = 0;
const LEFT = 1;
const UP = 2;
const RIGHT = 3;

const AXIS = 2;

const YAXIS = 0;
const XAXIS = 1;

const RANDOMAI = 0;
const SMOOTHRANDOMAI = 1;

const RECT = 0;
const CIRC = 1;
