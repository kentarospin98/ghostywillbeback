class Enemy {
  constructor(character, ai) {
    this.character = character;
    this.ai = ai;

    this.ailibrary;
    this.data;   // Data for the AI to use
  }

  show(board) {
    this.character.show(board);
  }

  update(board) {
    if (this.ai == RANDOMAI) {
      this.character.nextDirection = random([DOWN, LEFT, UP, RIGHT]);
      //this.data.runai = this.ailibrary.randomai;
    } else if (this.ai == SMOOTHRANDOMAI) {
      if (this.character.incell()) {
        let opendirections = getopendirections(round(this.character.x), round(this.character.y), board);
        if (opendirections.length != 2 | (opendirections.length == 2 && !opendirections.includes(this.character.currentDirection))) {
          this.character.nextDirection = random(opendirections);
        }
      }
      //this.data.runai = this.ailibrary.smoothrandomai;
    }
    //this.data.runai(board);
    this.character.update(board);
  }
}
