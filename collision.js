function checkcollision(box0, box1) {
  let collision = false;
  for (var i = 0; i < box0.length; i++) {
    for (var j = 0; j < box1.length; j++) {
      if (box0[i].type == RECT && box1[j].type == RECT) {
        collision = checkcollisionRectRect(box0[i], box1[j]) ? true : collision;
      } else if (box0[i].type == RECT, box1[j].type == CIRC) {
        collision = checkcollisionRectCirc(box0[i], box1[j]) ? true : collision;
      } else if (box0[i].type == CIRC, box1[j].type == RECT) {
        collision = checkcollisionRectCirc(box1[j], box0[i]) ? true : collision;
      }
    }
  }
  return collision;
}

function checkcollisionRectRect(box0, box1) {
  if (box0.x < box1.x + box1.width &&
      box0.x + box0.width > box1.x &&
      box0.y < box1.y + box1.height &&
      box0.y + box0.height > box1.y) {
    return true;
  }
}

function checkcollisionRectCirc(box, circ) {
  let circbox = circtorect(circ);
  return checkcollisionRectRect(circbox, box);
}

function inEllpise(x, y, circ) {
  return sq(x/circ.width , 2) + sq(y/circ.height, 2) < 1;
}

function circtorect(circ) {
  return {
    type : RECT,
    x : circ.x - circ.width/2,
    y : circ.y - circ.height/2,
    width : circ.width,
    height : circ.height
  };
}
