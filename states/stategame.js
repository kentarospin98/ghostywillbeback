class Stategame {
  constructor() {
    return 0;
  }

  update(data) {
    return 0;
  }

  show(data) {
    push()
    background(data.board.bg) // Clear sceen with board's background color

    data.ghosty.update(data.board); // Update ghosty
    data.ghosty.show(data.board);   // Render ghosty

    for (var i = 0; i < data.enemies.length; i++) {
      data.enemies[i].update(data.board);

      if (checkcollision(data.ghosty.character.getBoundingBoxes(data.board), data.enemies[i].character.getBoundingBoxes(data.board))){
        noLoop();
      }

      data.enemies[i].show(data.board);
    }

    for (var i = 0; i < data.pellets.length; i++) {
      if (!data.pellets[i].eaten && checkcollision(data.ghosty.character.getBoundingBoxes(data.board), data.pellets[i].getBoundingBoxes(data.board))) {
        data.pellets[i].eat();
        data.ghosty.score++;
      }

      data.pellets[i].update(data.board);
      data.pellets[i].show(data.board);
    }

    data.board.show();

    pop()
  }

  startState(data) {
    return 0;
  }

  endState(data) {
    return 0;
  }

  keyPressed(data, event) {
    switch (event.key) { // Set ghosty's nextDirection based on input
      case "ArrowUp":
        data.ghosty.character.nextDirection = UP;
        break;
      case "ArrowRight":
        data.ghosty.character.nextDirection = RIGHT;
        break;
      case "ArrowDown":
        data.ghosty.character.nextDirection = DOWN;
        break;
      case "ArrowLeft":
        data.ghosty.character.nextDirection = LEFT;
        break;
    }
    // if (debug) {
    //   console.log(event.key);
    //   let x = floor(mouseX/board.cellw)
    //   let y = floor(mouseY/board.cellh)
    //   let direction;
    //   switch (event.key) {
    //     case "w":
    //       direction = UP;
    //       break;
    //     case "d":
    //       direction = RIGHT;
    //       break;
    //     case "s":
    //       direction = DOWN;
    //       break;
    //     case "a":
    //       direction = LEFT;
    //       break;
    //     default:
    //       direction = null;
    //   }
    //   if (direction){
    //     board.breakCellWall(x, y, direction)
    //   }
    // }
  }
}
