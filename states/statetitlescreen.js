class Statetitlescreen {
  constructor() {
    return 0;
  }

  update(data) {
    return 0;
  }

  show(data) {
    background(0)
    fill(255);
    textSize(32);
    textAlign(CENTER);
    textFont( data.fonts["retroscape"]);
    text("GHOSTY WILL BE BACK", width/2, height/4);
    text("START GAME", width/2, height/2);
    // noLoop();
  }

  startState(data) {
    push();
    if (!data.fonts["retroscape"]) {
      data.fonts["retroscape"] = "serif"
      data.fonts["retroscape"] = loadFont("fonts/Retroscape.ttf");
    }
    return 0;
  }

  endState(data) {
    pop();
    return 0;
  }

  keyPressed(data, event) {
    switch (event.key) {
      case "Enter":
          statemachine.changeState("game")
        break;
      default:

    }
  }
}
