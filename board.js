class Board {
  // Set the rows, cols, and dimensions based on parameters
  constructor(level) {
    this.cols = level.cols;
    this.rows = level.rows;

    this.cellw = width/level.cols;
    this.cellh = width/level.rows;

    // Set the background color and initialize grid as an empty array
    this.bg = color(level.bg[0],level.bg[1],level.bg[2],level.bg[3]);
    this.grid = level.grid;
  }

  // Function to generate the grid
  generateGrid() {
    for (let i = 0; i < cols; i++) { // Iterate through columns
      let col = [] // Initialize a empty column array
      for (let j = 0; j < rows; j++) { // Iterate through rows
        col.push([true, true, true, true]) // Add a cell to column with walls as : Down, Left, Up, Right
      }
      this.grid.push(col) // Add the column to drid
    }
  }

  show() {
    strokeWeight(1);
    stroke(255)
    for (var i = 0; i < this.grid.length; i++) { // Iterate through columns
      let col = this.grid[i]
      for (var j = 0; j < col.length; j++) { // Iterare through cells (rows)
        let cell = col[j];
        // Draw Cell Walls
        if (cell[DOWN]) {
          line( i       * this.cellw, (j + 1) * this.cellh, (i + 1) * this.cellw, (j + 1) * this.cellh);
        }
        if (cell[LEFT]) {
          line( i       * this.cellw,  j      * this.cellh,  i      * this.cellw, (j + 1) * this.cellh);
        }
        if (cell[UP]) {
          line( i       * this.cellw,  j      * this.cellh, (i + 1) * this.cellw,  j      * this.cellh);
        }
        if (cell[RIGHT]) {
          line( (i + 1) * this.cellw,  j      * this.cellh, (i + 1)  * this.cellw, (j + 1) * this.cellh);
        }
      }
    }
  }

  // Return cell at (x, y)
  getCell(x, y) {
    return this.grid[round(x)][round(y)];
  }

  // Return if a wall exists in the direction of `direction` in cell at (x, y)
  getCellWall(x, y, direction) {
    if (x >= 0 && x < this.cols && y >= 0 && y < this.rows) {
      return this.getCell(x, y)[direction];
    } else {
      if (x > this.cols - 1) {
        // Return a wall if not on x-axis
        return direction % AXIS != XAXIS;
      } else if (y > this.rows - 1) {
        // Return a wall if not on y-axis
        return direction % AXIS == YAXIS;
      } else {
        // Otherwise return no wall
        return false;
      }
    }
  }

  // Builds a cell wall at (x, y) in a particular direction
  buildCellWall(x, y, direction) {
    this.getCell(x, y, direction)[direction] = true;
    this.getCell(x + (direction - 2) % 2, y + (1 - direction) % 2)[(direction + 2) % 4] = true; // Builds the was on the next cell too
  }

  // Breaks a cell wall at (x, y) in a particular direction
  breakCellWall(x, y, direction) {
      this.getCell(x, y, direction)[direction] = false;
      this.getCell(x + (direction - 2) % 2, y + (1 - direction) % 2)[(direction + 2) % 4] = false; // Breaks the was on the next cell too
  }
}
