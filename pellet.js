class Pellet {
  constructor(x, y, width, height) {
    this.x = x;
    this.y = y;

    this.width = width;
    this.height = height;

    this.boundingw = 0.2;
    this.boundingh = 0.2;

    this.eaten = false;
  }

  update(board) {
    return 0;
  }

  show(board) {
    fill(255);
    if (!this.eaten) {
      ellipse(this.x*board.cellw, this.y*board.cellh, this.width, this.height);
    }
  }

  getBoundingBoxes(board) {
    let boundingBoxes = [];
    boundingBoxes.push(circtorect({
      type : CIRC,
      x : this.x,
      y : this.y,
      width : this.boundingw,
      height : this.boundingh
    }));
    return boundingBoxes;
  }

  eat() {
    this.eaten = true;
  }
}
