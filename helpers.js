function getopendirections(x, y, board) {
  let opendirections = [];
  for (var i = 0; i < 4; i++) {
    if (!board.grid[x][y][i]){
      opendirections.push(i);
    }
  }
  return opendirections;
}
