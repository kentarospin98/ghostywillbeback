class Ghosty {
  constructor(character, data) {
    this.character = character;
    this.name = name;

    this.score = data.score != null ? data.score : 0;
    this.lives = data.lives != null ? data.lives : 3;
  }

  update(board) {
    this.character.update(board);
  }

  show(board) {
    this.character.show(board);
  }
}
