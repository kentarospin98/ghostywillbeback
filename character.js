class Character {
  constructor(x, y, width, height) {
    // Set the x, y, and dimensions based on parameters
    this.width = width;   // Width on screen in pixels
    this.height = height; // Height on screen in pixels

    this.x = x;
    this.y = y;

    this.boundingw = 0.7;
    this.boundingh = 0.7;

    // Initialize the velocity
    this.vel = 0.1;

    // Initialize the direction
    this.currentDirection = LEFT;
    this.nextDirection = UP;
  }

  // Render ghosty
  show(board) {
    strokeWeight(0);
    rect(this.x * board.cellw + this.width * (1-this.boundingw)/2, this.y * board.cellh + this.height * (1-this.boundingh)/2, this.boundingw *  this.width, this.boundingh * this.height);
    var shadow = {draw : false, x : this.x, y : this.y};
    if (this.x > board.cols - 1) {
      shadow.draw = true;
      shadow.x = this.x - board.cols
    }
    if (this.y > board.rows - 1) {
      shadow.draw = true;
      shadow.y = this.y - board.rows
    }
    rect(shadow.x * board.cellw + this.width * (1-this.boundingw)/2, shadow.y * board.cellh + this.height * (1-this.boundingh)/2, this.boundingw *  this.width, this.boundingh * this.height);
  }

  getBoundingBoxes(board) {
    let boundingBoxes = [];
    boundingBoxes.push({
      type : RECT,
      x : this.x + (1-this.boundingw)/2,
      y : this.y + (1-this.boundingh)/2,
      width : this.boundingw,
      height : this.boundingh
    });
    if (this.y > board.rows - 1) {
      boundingBoxes.push({
        x : this.x + (1-this.boundingw)/2,
        y : this.y - board.rows + (1-this.boundingh)/2,
        width : this.boundingw,
        height : this.boundingh
      });
    }
    if (this.x > board.cols - 1) {
      boundingBoxes.push({
        x : this.x - board.cols + (1-this.boundingw)/2,
        y : this.y + (1-this.boundingh)/2,
        width : this.boundingw,
        height : this.boundingh
      });
    }
    return boundingBoxes;
  }

  // Snap ghosty exactly to grid
  snapToGrid() {
    this.x = round(this.x)
    this.y = round(this.y)
  }

  incell() {
    return this.x % 1 < this.vel && this.y % 1 < this.vel;
  }

  update(board) {
    // Check for certian conditions
    let diffDirection = this.currentDirection != this.nextDirection;
    let sameAxis = this.currentDirection % AXIS == this.nextDirection % AXIS;
    let inCell = this.incell();

    // If inbetween cells or if in a cell and not blocked by a wall then move or snap to grid
    if (!inCell || (inCell && !board.getCellWall(round(this.x), round(this.y), this.currentDirection))) {
      this.x +=  (this.currentDirection - 2) * (this.currentDirection % 2) * this.vel;
      this.x = (this.x + board.cols) % board.rows;
      this.y +=  (this.currentDirection - 1) * (this.currentDirection % 2 - 1) * this.vel;
    } else {
      // else if stopped, snap ghosty to grid
      this.snapToGrid();
    }

    // If there is no 90 degree turn or if in a cell
    if (diffDirection || sameAxis || inCell) {
      // If in a cell and not blocked bt a wal in the nextDirection or turning in the same axis
      if (diffDirection && ((inCell && !board.getCellWall(round(this.x), round(this.y), this.nextDirection)) || sameAxis)) {
        // Change direction
        this.currentDirection = this.nextDirection;
      }
      // If turning and not blocked by a wall while in a cell
      if (inCell && !sameAxis && !board.getCellWall(round(this.x), round(this.y), this.nextDirection)) {
        // Snap ghosty to grid
        this.snapToGrid();
      }
    }
  }

}
