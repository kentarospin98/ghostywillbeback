// let ghosty;
// let enemies;
// let board;
// let pellets;
let levels;
let debug;

let statemachine;


function preload() {
  let levellist = ["/levels/0.json"]
  levels = [];
  for (var i = 0; i < levellist.length; i++) {
    levels.push(loadJSON(levellist[i]));
  }
}

function setup() {
  createCanvas(640, 640);
  background(0);

  cols = levels[0].rows; // Set number of rows for grid
  rows = levels[0].cols; // Set number of columns for grid

  debug = true; // Set debug as true

  let states = ["titlescreen", "game"]

  statemachine = new StateMachine(states, {
    "titlescreen" : new Statetitlescreen(),
    "game" : new Stategame()
  });

  statemachine.data.ghosty = new Ghosty(new Character(3, 3, width/cols, height/rows), { score : 0, lives : 3}); // Create a new ghosty and set his size based on grid
  statemachine.data.board = new Board(levels[0]) // Create new board and set its dimensions and color

  statemachine.data.enemies = [];
  for (var i = 0; i < levels[0].bots.length; i++) {
    statemachine.data.enemies.push(new Enemy(new Character(levels[0].bots[i].x, levels[0].bots[i].y, width/cols, height/rows), levels[0].bots[i].type))
  }

  statemachine.data.pellets = [];
  for (var i = 0; i < cols; i++) {
    for (var j = 0; j < rows; j++) {
      statemachine.data.pellets.push(new Pellet(i + 0.5, j + 0.5, statemachine.data.board.cellw/4, statemachine.data.board.cellh/4));
    }
  }

  statemachine.changeState("titlescreen")

}

function draw() {
  statemachine.update();
  statemachine.show();
}

function keyPressed(event) {
  statemachine.keyPressed(event);
}
