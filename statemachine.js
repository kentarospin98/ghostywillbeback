class StateMachine {
  constructor(states, statefuncs) {
    this.state = "";
    this.states = states;
    this.statefuncs = statefuncs;
    this.data = {
      fonts : {}
    };
  }

  startState(state) {
    if (this.statefuncs[state].startState) {
      this.statefuncs[state].startState(this.data);
    }
  }

  endState(state) {
    if (this.statefuncs[state].endState) {
      this.statefuncs[state].endState(this.data);
    }
  }

  changeState(state) {
    if (this.states.includes(state)) {
      if (this.statefuncs[this.state]) {
        this.endState(this.state);
      }
      this.state = state;
      this.startState(this.state);
    }
  }

  update() {
    this.statefuncs[this.state].update(this.data);
  }

  show() {
    this.statefuncs[this.state].show(this.data);
  }

  keyPressed(event) {
    this.statefuncs[this.state].keyPressed(this.data, event);
  }
}
